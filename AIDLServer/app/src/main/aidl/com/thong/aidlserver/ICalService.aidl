// ICalService.aidl
package com.thong.aidlserver;

// Declare any non-default types here with import statements

interface ICalService {
    String getMessage(String name);
    int getResult(int val1, int val2);
}

