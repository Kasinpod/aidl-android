package com.thong.aidlserver

import android.app.Service
import android.content.Intent
import android.os.IBinder

class CalService : Service() {

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    private val binder: ICalService.Stub = object : ICalService.Stub() {
        override fun getMessage(name: String?): String {
            return "Hello $name, Result is:"
        }

        override fun getResult(val1: Int, val2: Int): Int {
            return val1 * val2
        }

    }
}