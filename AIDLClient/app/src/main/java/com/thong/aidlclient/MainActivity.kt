package com.thong.aidlclient

import android.app.Service
import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import androidx.appcompat.app.AppCompatActivity
import com.thong.aidlserver.ICalService
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private var calService: ICalService? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        onEvent()
    }

    private fun onEvent() {
        multiply_btn.setOnClickListener {
            val num1: Int = num1.text.toString().toInt()
            val num2: Int = num2.text.toString().toInt()
            try {
                val result = calService!!.getResult(num1, num2)
                val msg = calService!!.getMessage(name.text.toString())

                text_result.text = msg + result
            } catch (e: RemoteException) {
                e.printStackTrace()
            }

        }
    }

    private val connection = object : ServiceConnection {
        override fun onServiceDisconnected(name: ComponentName?) {
            calService = null
        }

        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            calService = ICalService.Stub.asInterface(service)
        }

    }

    override fun onStart() {
        super.onStart()
        if (calService == null){
            val intent = Intent("multiplyservice")
            intent.setPackage("com.thong.aidlserver")
            bindService(intent,connection, Service.BIND_AUTO_CREATE)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        unbindService(connection)
    }
}
